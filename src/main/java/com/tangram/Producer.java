package com.tangram;

import javax.jms.DeliveryMode;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.tangram.common.EmbeddedBrokerService;
import com.tangram.common.SimpleJmsConnectionFactory;
import org.apache.activemq.broker.BrokerService;

public class Producer {
    public static void main(String[] args) throws Exception {
        new Producer();
    }

    public Producer() throws Exception {
        QueueConnection conn=(new SimpleJmsConnectionFactory("tcp://localhost:61616?trace=true"))
                .getQueueConnection();

        QueueSession session=conn.createQueueSession(false,Session.AUTO_ACKNOWLEDGE);
        conn.start();

        Queue sendQueue=session.createQueue("Queue1");
        QueueSender sender=session.createSender(sendQueue);

        for(String x : new String[]{"hi", "bye"}) {
            TextMessage msg=session.createTextMessage();
            msg.setText(x + Math.random());
            sender.send(msg,DeliveryMode.PERSISTENT,
                    javax.jms.Message.DEFAULT_PRIORITY,6000000);
        }

        conn.close();

        /*EmbeddedBrokerService svc = EmbeddedBrokerService.instance;
        BrokerService bsvc = svc.getBroker();
        bsvc.stop();*/
    }
}
