package com.tangram;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;

import com.tangram.common.EmbeddedBrokerService;
import com.tangram.common.SimpleJmsConnectionFactory;

public class Consumer implements MessageListener {

    private final QueueSession session;

    public static void main(String[] args) throws Exception {
        new Consumer();
    }

    public Consumer() throws Exception {
// activate embedded broker
        EmbeddedBrokerService.instance.init("tcp://localhost:61616?trace=true");

        QueueConnection conn=(new SimpleJmsConnectionFactory("tcp://localhost:61616?trace=true"))
                .getQueueConnection();

        session = conn.createQueueSession(true, Session.AUTO_ACKNOWLEDGE);

        conn.start();

        Queue receiveQueue= session.createQueue("Queue1");
        QueueReceiver qReceiver= session.createReceiver(receiveQueue);
        qReceiver.setMessageListener(this);

        BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in));
        while(true) {
            System.out.print(":>");
            String s=stdin.readLine();
            if(s.equalsIgnoreCase("exit")) {
                conn.close();
                System.exit(0);
            }
        }
    }

    public void onMessage(Message msg) {
        try {
            TextMessage textMessage=(TextMessage)msg;
            try {
                String string=textMessage.getText();
                System.out.println(string);
                session.rollback();
            } catch(JMSException e) {
                e.printStackTrace();
            }
        } catch(Throwable t) {
            t.printStackTrace();
        }
    }
}
