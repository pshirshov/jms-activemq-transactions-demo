package com.tangram.common;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.RedeliveryPolicy;


/*

Original demo:
http://java-and-oracle.blogspot.com/2006/04/jms-example-using-activemq.html

http://activemq.apache.org/message-redelivery-and-dlq-handling.html
http://activemq.apache.org/redelivery-policy.html
http://activemq.apache.org/how-do-transactions-work.html

complex transactions demo: http://docs.oracle.com/javaee/1.3/jms/tutorial/1_3_1-fcs/examples/appendix/TransactedExample.java

interesting: http://activemq.apache.org/delay-and-schedule-message-delivery.html


Some info about jta:
http://docs.oracle.com/cd/E19717-01/819-7758/gbxro/index.html
http://stackoverflow.com/questions/517100/jms-rollback
http://docs.oracle.com/javaee/1.3/jms/tutorial/1_3_1-fcs/doc/advanced.html#1026538
 */
public class SimpleJmsConnectionFactory implements JmsConnectionFactory {
    private ConnectionFactory factory;

    public SimpleJmsConnectionFactory(String brokerUrl) {
        factory=new ActiveMQConnectionFactory(brokerUrl);

        if(brokerUrl.startsWith("vm://")) {
            ActiveMQConnectionFactory aqFactory=(ActiveMQConnectionFactory)factory;
            aqFactory.setObjectMessageSerializationDefered(true);
            aqFactory.setDispatchAsync(true);
            aqFactory.setUseAsyncSend(true);
        }
    }

    protected Connection getConnection() throws JMSException {
        Connection connection = factory.createConnection();

        RedeliveryPolicy policy = ((ActiveMQConnection) connection).getRedeliveryPolicy();

        policy.setInitialRedeliveryDelay(500);
        policy.setBackOffMultiplier((short) 2);
        policy.setUseExponentialBackOff(false);
        policy.setMaximumRedeliveries(-1);

        return connection;
    }

    public QueueConnection getQueueConnection() throws JMSException {
        return (QueueConnection)getConnection();
    }

    public TopicConnection getTopicConnection() throws JMSException {
        return (TopicConnection)getConnection();
    }
}