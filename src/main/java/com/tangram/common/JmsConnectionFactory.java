package com.tangram.common;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.TopicConnection;

public interface JmsConnectionFactory {
    public QueueConnection getQueueConnection() throws JMSException;
    public TopicConnection getTopicConnection() throws JMSException;
}

