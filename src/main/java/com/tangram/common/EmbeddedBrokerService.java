package com.tangram.common;

//import oracle.jdbc.pool.OracleDataSource;
import org.h2.jdbcx.JdbcDataSource;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.store.jdbc.JDBCPersistenceAdapter;

public class EmbeddedBrokerService {
    public static final EmbeddedBrokerService instance=new EmbeddedBrokerService();
    protected BrokerService broker;

    public BrokerService getBroker() {
        return broker;
    }

    public void init(String url) {
        broker=new BrokerService();
        try {
            /*OracleDataSource ods=new OracleDataSource();
            ods.setURL("jdbc:oracle:thin:@10.0.0.1:1521:orcl");
            ods.setUser("activemq");
            ods.setPassword("activemq");*/

            JdbcDataSource ds = new JdbcDataSource();
            ds.setURL("jdbc:h2:~/test");
            JDBCPersistenceAdapter persAd=new JDBCPersistenceAdapter();
            persAd.setDataSource(ds);

            broker.setPersistenceAdapter(persAd);
            broker.setPersistent(true);
            broker.addConnector(url);

            broker.start();
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected EmbeddedBrokerService() {}
}